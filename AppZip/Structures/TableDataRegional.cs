﻿namespace AppZip.Structures
{
    public class TableDataRegional
    {
        public string Marca { get; set; }
        public string Codigo_concessionaria { get; set; }
        public string Nome_concessionaria { get; set; }
        public string Arquivo { get; set; }
        public string Regional { get; set; }
        public string Estagiario { get; set; }
        public string Analista { get; set; }
        public string Coordenador_1 { get; set; }
        public string Coordenador_2 { get; set; }
        public string Coordenador_3 { get; set; }
    }
}

﻿namespace AppZip.Structures
{
    public class TableDataEmail
    {
        public string Marca { get; set; }
        public string Codigo_concessionaria { get; set; }
        public string Nome_concessionaria { get; set; }
        public string Arquivo { get; set; }
        public string Regional { get; set; }
        public string Grupo { get; set; }
        public int Repetidor { get; set; }



    }
}

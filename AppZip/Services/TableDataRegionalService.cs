﻿using AppZip.Interfaces;
using AppZip.Structures;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace AppZip.Services
{
    public class TableDataRegionalService : ITableData<TableDataRegional>
    {
        public Task<string> Generate()
        {
            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;


            return Task.Run(() =>
            {
                var result = RESULT_STATUS.INITIAL;

                var listBase = CreateList();

                result = ZipFiles(listBase, result);

                return result;
            });
        }

        private List<TableDataRegional> CreateList()
        {
            var _fileInfo = new FileInfo(PATH_VALUES.BASE_REGIONAL);

            List<TableDataRegional> listBase = new List<TableDataRegional>();

            using (var package = new ExcelPackage(_fileInfo))
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.First();

                var _Tables = worksheet.Tables;

                var Base = _Tables[0];

                for (int i = 2; i <= Base.Range.Rows; i++)
                {
                    listBase.Add(new TableDataRegional
                    {
                        Marca = (string)worksheet.Cells[i, 1].Value,
                        Codigo_concessionaria = Convert.ToString(worksheet.Cells[i, 2].Value),
                        Nome_concessionaria = (string)worksheet.Cells[i, 3].Value,
                        Arquivo = (string)worksheet.Cells[i, 4].Value,
                        Regional = (string)worksheet.Cells[i, 5].Value,
                        Estagiario = (string)worksheet.Cells[i, 6].Value,
                        Analista = (string)worksheet.Cells[i, 7].Value,
                        Coordenador_1 = (string)worksheet.Cells[i, 8].Value,
                        Coordenador_2 = (string)worksheet.Cells[i, 9].Value,
                        Coordenador_3 = (string)worksheet.Cells[i, 10].Value
                    });
                }
            }

            return listBase;
        }

        private string ZipFiles(List<TableDataRegional> listBase, string result)
        {

            if (Directory.Exists(PATH_VALUES.PATH))
            {
                result = RESULT_STATUS.DIRECTORY_EXISTS;

                foreach (var i in listBase)
                {
                    string _zipName = $"{PATH_VALUES.PATH}\\{i.Regional}.zip";

                    if (!Directory.GetFiles(PATH_VALUES.PATH, $"*{i.Regional}*.*").Any())
                    {
                        using (ZipArchive newFile = ZipFile.Open(_zipName, ZipArchiveMode.Create))
                        {
                            foreach (var item in listBase)
                            {
                                var directoryList = Directory.GetFiles(PATH_VALUES.PATH, $"*{item.Codigo_concessionaria}*.*");

                                string directoryName = $"{PATH_VALUES.PATH}\\{directoryList[0]}";

                                if (item.Regional == i.Regional)
                                {
                                    foreach (string file in directoryList)
                                    {
                                        Console.WriteLine($"Adicionando arquivo {file} em {_zipName}");
                                        newFile.CreateEntryFromFile(file, System.IO.Path.GetFileName(file));
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                result = RESULT_STATUS.DIRECTORY_DO_NOT_EXISTS;
            }

            return result;
        }
    }
}

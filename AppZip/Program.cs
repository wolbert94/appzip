﻿using AppZip.Interfaces;
using AppZip.Services;
using AppZip.Structures;

namespace AppZip
{
    class Program
    {
        static void Main(string[] args)
        {
            ITableData<TableDataEmail> emailTable = new TableDataEmailService();

            ITableData<TableDataRegional> regionalTable = new TableDataRegionalService();

            emailTable.Generate();

            regionalTable.Generate();

        }
    }
}

﻿using System.Threading.Tasks;

namespace AppZip.Interfaces
{
    public interface ITableData<T>
    {
        public Task<string> Generate();
    }
}
